const fetch = require("node-fetch");
const { ApolloServer, gql } = require("apollo-server");


const typeDefs = gql`
  # Comments in GraphQL are defined with the hash (#) symbol.

  # This "categoria" type can be used in other type declarations.
  type Categoria {
    id: ID
    nombrecategoria: String
    tipocategoria: String
  }

  # The "Query" type is the root of all GraphQL queries.
  # (A "Mutation" type will be covered later on.)

  type Query {
    categoria(id : ID!): Categoria
    categorias: [Categoria]
  }
`;

const fetchCategoria = () => {
	return fetch('http://54.147.192.195:33061/categorias')
	.then(res => res.json())
}
const fetchUnaCategoria = (id) => {
        return fetch("http://54.147.192.195:33061/categoria/" + id )
        .then(res => res.json())
}
// Resolvers define the technique for fetching the types in the
// schema.  We'll retrieve books from the "books" array above.
const resolvers = {
  Query: {
    categorias: () => fetchCategoria(),
    categoria: (parent , args )=> {
      const {id} = args
      return fetchUnaCategoria({id})
  }
  }
};

// In the most basic sense, the ApolloServer can be started
// by passing type definitions (typeDefs) and the resolvers
// responsible for fetching the data for those types.
const server = new ApolloServer({ typeDefs, resolvers });

// This `listen` method launches a web-server.  Existing apps
// can utilize middleware options, which we'll discuss later.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
